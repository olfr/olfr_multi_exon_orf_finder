package Mods::GetAltMets;

use strict;
use warnings;
use Data::Dumper;

sub AltMets {
  my($me_orfs, $min_len) = @_;
  foreach my $trans( @{ $me_orfs } ){
    next if(not $trans->[7]->{'split_cds'}); # get next trans if there are no splice sites or if the splice sites are outside the potential ORF
    my $pep_size = $trans->[7]->{'peplen'};
    my $first_met_pos = $trans->[7]->{'tindx_start'};
    my@split_pep = split"",$trans->[7]->{'lmpep'}; 
    for(my$i=0;$i<@split_pep;$i++){
      if($split_pep[$i] eq 'M' && $pep_size - $i > $min_len){ # only add it if the peptide starting at this Met is > min_len (300aa default)
        push@{ $trans->[7]->{'met_index'} },[$i, ($i * 3) + $first_met_pos]; # index positions for Mets in the peptide and the transcript
      }
    }
    next unless exists($trans->[7]->{'met_index'}) && @{ $trans->[5] };
    $trans->[7]->{'met_summary'} = undef;
    my (%met_summary, $met_summary_st);
    for(my$i=0;$i<@{ $trans->[7]->{'met_index'} };$i++){
      for(my$ss=0;$ss<@{ $trans->[5] };$ss++){
        # "$trans->[5]->[$ss] - 1" used this because $ss is using 1 based index whereas the 'met_index' is 0 based indexed
        if(not ($trans->[7]->{'met_index'}->[$i]->[1] < $trans->[5]->[$ss] - 1 and $trans->[5]->[$ss] < $trans->[7]->{'tindx_end'})){
          push@{ $trans->[7]->{'met_index'}->[$i] }, 'ns'; # ORF is not split between exons
          $met_summary{ $i }{ 'ns' }++;
        } else {
          push@{ $trans->[7]->{'met_index'}->[$i] }, 's'; # ORF is potentially split between exons
          $met_summary{ $i }{ 's' }++;
          if(not $trans->[7]->{'met_index'}->[$i]->[1]){ # the ATG is at the start (0 - index) of the transcript
            $trans->[4]=~s/(^\w{3})/\[$1\]/;
          } else {
            my $trans_met_offset = $trans->[7]->{'met_index'}->[$i]->[1];
            my @bracket_num;
            if($trans->[4] =~m/\[+/){
              @bracket_num = $trans->[4] =~m/(\[+)/g;
            }
            $trans_met_offset += $ss;
            $trans_met_offset += @bracket_num*2;
            $trans->[4] =~s/([\[\]\w\|]{$trans_met_offset})(\w{1}\|*\w{1}\|*\w{1})/$1\[$2\]/;
          }
          last;
        }
      }
    }
    foreach my $met_index(sort{ $a <=> $b } keys %met_summary){
      if(exists($met_summary{$met_index}{'s'})){
        $met_summary_st .= join':', $met_index + 1, 'MEO', $trans->[7]->{'met_index'}->[$met_index]->[1] + 1; # multi-exon-ORF if this is the true start-Met ( start index= 1 )
      }
      else {
        $met_summary_st .= join':', $met_index + 1, 'SEO', $trans->[7]->{'met_index'}->[$met_index]->[1] + 1; # single-exon-ORF if this is the true start-Met ( start index= 1 ) 
      }
      $met_summary_st .= q{|};
    }
    $met_summary_st=~s/\|$//;
    $trans->[7]->{'met_summary'} = $met_summary_st; 
  }
  return $me_orfs;
}

1;
