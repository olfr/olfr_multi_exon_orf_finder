package Mods::GetSpliceSites;

use strict;
use warnings;
use Data::Dumper;

my %GC = (
      'ATA'=>'I', 'ATC'=>'I', 'ATT'=>'I', 'ATG'=>'M',
      'ACA'=>'T', 'ACC'=>'T', 'ACG'=>'T', 'ACT'=>'T',
      'AAC'=>'N', 'AAT'=>'N', 'AAA'=>'K', 'AAG'=>'K',
      'AGC'=>'S', 'AGT'=>'S', 'AGA'=>'R', 'AGG'=>'R',
      'CTA'=>'L', 'CTC'=>'L', 'CTG'=>'L', 'CTT'=>'L',
      'CCA'=>'P', 'CCC'=>'P', 'CCG'=>'P', 'CCT'=>'P',
      'CAC'=>'H', 'CAT'=>'H', 'CAA'=>'Q', 'CAG'=>'Q',
      'CGA'=>'R', 'CGC'=>'R', 'CGG'=>'R', 'CGT'=>'R',
      'GTA'=>'V', 'GTC'=>'V', 'GTG'=>'V', 'GTT'=>'V',
      'GCA'=>'A', 'GCC'=>'A', 'GCG'=>'A', 'GCT'=>'A',
      'GAC'=>'D', 'GAT'=>'D', 'GAA'=>'E', 'GAG'=>'E',
      'GGA'=>'G', 'GGC'=>'G', 'GGG'=>'G', 'GGT'=>'G',
      'TCA'=>'S', 'TCC'=>'S', 'TCG'=>'S', 'TCT'=>'S',
      'TTC'=>'F', 'TTT'=>'F', 'TTA'=>'L', 'TTG'=>'L',
      'TAC'=>'Y', 'TAT'=>'Y', 'TAA'=>'*', 'TAG'=>'*',
      'TGC'=>'C', 'TGT'=>'C', 'TGA'=>'*', 'TGG'=>'W'
);

sub SpliceSites {
  my ($dna, $min_len) = @_;
  die "need an input dna string\n" unless $dna;
  foreach my $trans(@$dna){
    my (@P,%LP);
    print STDERR join("\t", $trans->[1], $trans->[0]), "\r";
    my $seq = $trans->[4];
    $seq=~s/\|//g; # remove the splice site indicators
    foreach my $i(0,1,2){ # translate the transcript in 3 ORFs
      my $prot;
      for(my$j=$i;$j<length($seq);$j+=3){
        my $codon = substr($seq,$j,3);
        my $aa = exists($GC{ $codon }) ? $GC{ $codon } : 'X';
        $prot .= $aa;
      }
      push@P,$prot;
    }
    my $big = $min_len;
    for(my$i=0;$i<@P;$i++){ # find the longest peptide for each transcript
      my @SP = split'\*',$P[$i];
      foreach my $pep(@SP){
        my ($mpep) = $pep=~/(M[\w]+)/; # find the first (longest) sub-peptide starting with M
        $mpep=~s/X+$// if $mpep; # remove any C terminal X
        if($mpep && length($mpep) > $big){ # min size (default is 300aa)
          my $pindex = index($P[$i], $mpep);
          my $tindex = ($pindex * 3) + $i;
          $LP{ 'lmpep' } = $mpep; # longest peptide
          $LP{ 'frame' } = $i; # longest peptide reading frame
          $LP{ 'pindx' } = $pindex; # index of Met position of longest pep in protein coords
          $LP{ 'tindx_start' } = $tindex; # Met index of longest pep in transcript coord - first base of start Met = 0. If cds = AT|G; A = 0 and G = 3 and | = 2
          $LP{ 'tindx_end' } = $tindex + (length($mpep) * 3); # transcript coord of first base in stop codon or X 
          $big = length($mpep);
          $LP{ 'peplen' } = $big;
        }
      }
    }
    if(keys %LP && @{ $trans->[5] }){ # if the peptide is greater than a min aa size (default 300aa) AND the transcript is spliced
      # check to see if any of the splice sites occur within the longest ORF
      my $split_cds = 0;
      for(my$s=0;$s<@{ $trans->[5] };$s++){
        if($trans->[5]->[$s] > $LP{ 'tindx_start' } + 1 && $trans->[5]->[$s] < $LP{ 'tindx_end' }){
          $split_cds = 1;
        }
      }
      if($split_cds){
       $LP{ 'split_cds' } = 1; # potential splice site within the ORF
      } else {
       $LP{ 'split_cds' } = 0; # no splice site within the ORF - but the transcript is spliced
     }
    } else {
      $LP{ 'no_splice_or_small_pep' } = 1; # no splice site within the transcript or the peptide is smaller that the minimum cut-off size
    }
    push@$trans,\@P,\%LP;
  }
 return $dna; 
}

1;
 
