package Mods::GetTransSeq;

use strict;
use warnings;
use Data::Dumper;
use Carp;
use Bio::EnsEMBL::Registry;

my $registry = 'Bio::EnsEMBL::Registry';

$registry->load_registry_from_multiple_dbs( {
   '-host'    => 'ensembldb.ensembl.org',
   '-user'    => 'anonymous',
 },);

sub  TransSeq {
  my ($exon_file, $species) = @_;
  my $slice_a = Bio::EnsEMBL::Registry->get_adaptor("$species", "core", "Slice");
  my $karyo = kt($species);
  my (%Trans,@Return_Trans);
  if(open(my $fh, $exon_file)) {
    while(my $line = <$fh>){
      chomp $line;
       my($chr, $group, $from, $to, $strand, $anno) = (split("\t", $line))[0,1,3,4,6,8];
       if( not exists $karyo->{ $chr } or $to > $karyo->{ $chr } ){
         croak "are you sure the species is correct: $!"
       }
       my($gene_id, $trans_id, $exon_num, $gene_name, $transcript_name, $biotype) = 
         $anno =~/gene_id "(\w+).*transcript_id "(\w+).*exon_number "(\w+).*gene_name "(\w+).*transcript_name "([\w-]+).*transcript_biotype "(\w+)/;
       my $ex_slice = $slice_a->fetch_by_region('chromosome', $chr, $from, $to);
       next unless ($gene_id && $gene_name && $group && $trans_id && $transcript_name && $biotype);
       my $gene_iden = join':', $gene_id, $gene_name;
       my $trans_iden = join':', $group, $trans_id, $transcript_name, $biotype;
       my $loc = join':', $chr, $from, $to, $strand, $exon_num;
       my $seq = $strand eq '-' ? revcomp([ split'', $ex_slice->seq ]) : $ex_slice->seq;
       push@{ $Trans{ $gene_iden }{ $trans_iden }{ 'loc' } }, $loc;
       push@{ $Trans{ $gene_iden }{ $trans_iden }{ 'seq' } }, [$seq, $exon_num];
    }
  }
  my $counter = 1;
  foreach my $gene_id(keys %Trans){
    foreach my $trans_id(keys %{ $Trans{ $gene_id } }){
      my($loc,$seq,@TP);
      $loc = join'*', @{ $Trans{ $gene_id }{ $trans_id }{ 'loc' } };
      # sort the sequences by ascending exon number - they should already appear this way in the GTF file
      @{ $Trans{ $gene_id }{ $trans_id }{ 'seq' } } = map { $_->[0] } sort { $a->[1] <=> $b->[1] } @{ $Trans{ $gene_id }{ $trans_id }{ 'seq' } };
      $seq = join'|', @{ $Trans{ $gene_id }{ $trans_id }{ 'seq' } };
      # find the transcript position(s) of the splice site(s) - first base at 5' end of transcript has position = 1
      if($seq=~/\|/){
        my@S=split'',$seq;
        for(my$i=0;$i<@S;$i++){
          if($S[$i] eq '|'){
            push@TP, ( $i + 1 - @TP );
          }
        }
      }
      push@Return_Trans, [$counter, $gene_id, $trans_id, $loc, $seq, \@TP];
      $counter++; # counts the total number of transcripts
    }
  }
  return \@Return_Trans;
}

sub revcomp {
  my $seq = shift;
  my %comp = (
   'A'=>'T',
   'T'=>'A',
   'C'=>'G',
   'G'=>'C',
   'N'=>'N',
  );
  @$seq = reverse(@$seq);
  for(my$i=0;$i<@$seq;$i++){
    $seq->[$i] = $comp{ $seq->[$i] };
  }
  return join'',@{ $seq };
}

sub kt {
  my $species = shift;
  my $genome = Bio::EnsEMBL::Registry->get_adaptor( "$species", "core", "GenomeContainer" );
  my %chroms;
  foreach my $slice(@{ $genome->get_karyotype() }) {
    $chroms{ $slice->seq_region_name() } = $slice->seq_region_end ();
  }
  return \%chroms;
}

1;
