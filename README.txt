# purpose:

The perl script and associated modules can be used to indicate splice sites and potential start-Mets in transcript sequences. 
The indicated start-Mets will lie upstream from the splice site(s) and will still maintain an ORF encoding a peptide greater that a user-defined minimum peptide size.

# minimal usage:

   `perl olfr.pl -f mouse96.olfr.exons.tsv > output.txt`

# help message:

   `perl olfr.pl` 
Usage: olfr.pl -f <exon_file.tsv - a file> -s <Human - a string> -m <min peptide size - a number>

 An input exon file (-f) is required and optionally a species (-s) - the default species is mouse - and optionally a minimum peptide size - the default is 300 aa at olfr.pl line 14.
        main::help() called at olfr.pl line 22

# installation (on Ubuntu):  

   `sudo apt-get update`  
  
  -- install cpanm if you don't have it:   
  
   `sudo apt-get install cpanminus`  
   
  -- various perl modules will need to be installed:  
  
  - Ensembl (if it's the latest version that you want - olfr.pl uses the latest Ensembl release by default) and BioPerl:  
  
   `cd olfr_multi_exon_orf_finder`
   `wget ftp://ftp.ensembl.org/pub/ensembl-api.tar.gz`
   `wget https://cpan.metacpan.org/authors/id/C/CJ/CJFIELDS/BioPerl-1.6.924.tar.gz`  
   `tar -zxvf ensembl-api.tar.gz`  
   `tar -zxvf BioPerl-1.6.924.tar.gz`  
 
  -- install 'build-essential' and 'libmysqlclient-dev' packages as they will be needed for the DBI module installation:  
  
   `sudo apt-get install build-essential` 
   `sudo apt-get install libmysqlclient-dev`  
  
  - DBI:  
  
   `cpanm DBI`  
 
  - DBD::mysql  
  
   `cpanm DBD::mysql`  
 
 -- check to see if all required modules have been installed correctly:  
 
   `perl -I ~/perl5/lib/perl5/ -I ./ensembl/modules -I ./BioPerl-1.6.924 olfr.pl`  
 Usage: olfr.pl -f <exon_file.tsv - a file> -s <Human - a string> -m <min peptide size>

An input exon file (-f) is required and optionally a species (-s) - the default species is mouse - and optionally a minimum peptide size the default is 300 aa at olfr.pl line 16.
        main::help() called at olfr.pl line 24

# generating the input (an example):  

Mus_musculus.GRCm38.96.chr.gtf.gz was downloaded from Ensembl (ftp://ftp.ensembl.org/pub/release-96/gtf/mus_musculus/)  
  
   `zgrep -i olfr Mus_musculus.GRCm38.96.chr.gtf.gz > mouse96.olfrs.gtf`   
   `zgrep OR5BS1P Mus_musculus.GRCm38.96.chr.gtf.gz >> mouse96.olfrs.gtf`   
   `zgrep AC134333 Mus_musculus.GRCm38.96.chr.gtf.gz >> mouse96.olfrs.gtf`  
   `grep exon mouse96.olfrs.gtf > mouse96.olfr.exons.tsv`  

An example input file (mouse96.olfr.exons.tsv) is included in this repository.

# Description of the output:

Each row represents a transcript derived from the input file of exon coordinates (the input file was itself extracted from a GTF file - see above). 
Each row consists of 8 tab-separated columns :
 1). The genomic sequence corresponding to the 5' -> 3' direction of the spliced transcript (reverse complement if transcribed from the -ve strand).
     Potential start Mets upstream from an ORF-splitting splice site are indicated as [ATG]. Splice sites are indicated with a '|'. 
 2). Gene ID : name
 3). Transcript information
 4). Spliced transcript coordinates in genomic exon coordinates (starting from the 5' exon of the transcript (exon-1)). Each exon coordinate is separated by an asterisk. 
     The coordinates consist of the chromosome name, start, end, strand and exon number separated by ':'.
 5). The peptide sequence from the longest ORF.
 6). The size of the peptide in column 5.
 7). The position(s) of the splice site(s) in the transcript sequence in column 1 - the first base in the sequence has coordinate 1. 
     Non-sequence symbols (|[]) are not included in the numbering.
 8). Possible start-Mets which still result in a peptide >= the minimum peptide size - separated by a '|'. 
     MEO = multi exon ORF - if this is the correct start-Met - the splice site is down-stream and the ORF is split.
     SEO = single exon ORF - splice site is up-stream of this Met - if this is the start-Met then the ORF is not split.

An example output file (output.txt) is included in this repository (see the minimal usage section above).

# docker image of this software:

A docker image with all the software installed can be downloaded as follows:
   `docker pull stephennfitzgerald/olfr_multi_exon_orf_finder`  
and run as follows (assuming your input file name is "mouse96.olfr.exons.tsv" and the species is mouse):
   `docker run -v $PWD:/olfr/input -e "infile=input/mouse96.olfr.exons.tsv" <image_id>`  
to change the default species and/or minimum peptide size:
   `docker run -v $PWD:/olfr/input -e "infile=input/human96.tnf.exons.tsv" -e "species=human" -e "minpep=500" <image_id>`  

