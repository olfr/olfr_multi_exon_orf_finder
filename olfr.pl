#!/usr/bin/env perl
use strict;
use warnings;
use Carp;
use Data::Dumper;
use Getopt::Long;
use File::Spec;
use FindBin;
use lib $FindBin::Bin;
use Mods::GetTransSeq; 
use Mods::GetSpliceSites;
use Mods::GetAltMets;

sub help {
  print "Usage: olfr.pl -f <exon_file.tsv - a file> -s <Human - a string> -m <min peptide size>\n\n";
  Carp::croak "An input exon file (-f) is required and optionally a species (-s) - the default species is mouse - and optionally a minimum peptide size the default is 300 aa";
}

my $exon_file = undef;
my $species = 'Mouse'; # default to mouse
my $min_length = 300; # minimum AA length 


$ARGV[0] && GetOptions ("f=s" => \$exon_file, "s=s" => \$species, "m=s" => \$min_length) && $exon_file && -e $exon_file && -s $exon_file or help;

$exon_file = File::Spec->rel2abs($exon_file);

# get the Ensembl transcript sequences - with splice site info
my $transcripts = Mods::GetTransSeq::TransSeq($exon_file, ucfirst($species));

# get the potential multi-exon ORFs
my $met = Mods::GetSpliceSites::SpliceSites($transcripts, $min_length);

# mark transcripts with potential downstream start-Mets  
my $altmet = Mods::GetAltMets::AltMets($met, $min_length);

foreach my $trans(@{ $altmet }){
  if($trans->[-1]->{'split_cds'}){
    # print transcript_sequence, gene_name, transcript_info, exon_genomic_coords, longest_peptide, peptide_length, transcript_splice_site_coords, multi-exon-ORF-summary 
    print join("\t", $trans->[4], @{ $trans }[1..3], $trans->[7]->{ 'lmpep' }, $trans->[7]->{ 'peplen' }, join(',', @{ $trans->[5] }), $trans->[7]->{ 'met_summary' }), "\n";
  }
}
